#!/usr/bin/env ruby
require 'bundler/setup'
Bundler.require

parts = Squib.csv file: 'parts.csv'

Squib::Deck.new(cards: parts['range'].size) do
  background color: '#eee'

  text str: parts['range'],
       color: parts['color'], font: 'Helvetica,Bold 288',
       y: '0.75in', width: '2.75in', align: :center

  text str: parts['material'],
       color: parts['color'], font: 'Helvetica,Light 72',
       y: '2.5in', width: '2.75in', align: :center

  # save_png prefix: 'parts_'
  save_pdf file: 'parts.pdf', gap: 100
end

gaps = Squib.csv file: 'gaps.csv'

Squib::Deck.new(cards: gaps['range'].size) do
  background color: '#eee'

  text str: gaps['range'],
       color: '#090A19', font: 'Helvetica,Bold 360',
       y: '1.15in', width: '2.75in', align: :center

  # save_png prefix: 'gaps_'
  save_pdf file: 'gaps.pdf', gap: 100
end
